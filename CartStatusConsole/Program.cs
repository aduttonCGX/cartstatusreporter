﻿using System;
using System.Text;
using System.IO;
using CartStatusReporter.Utilities;
using CartStatusReporter;
using Renci.SshNet;
using System.Configuration;

namespace CartStatusConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string currentDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                string archiveDirectory = currentDirectory + "\\Archive\\" + DateTime.Now.ToString(@"yyyy\\MM\\dd");
                string archiveSourceFile = $"Source_{DateTime.Now.ToString(@"yyyyMMdd")}.txt";
                if (!Directory.Exists(archiveDirectory))
                    Directory.CreateDirectory(archiveDirectory);

                // SFTP Connection
                string privateKey = @"-----BEGIN RSA PRIVATE KEY-----
Proc-Type: 4,ENCRYPTED
DEK-Info: DES-EDE3-CBC,E1D33410647C2F21

6l+7iLBfNECZkzmbvvDR2hrJbJOKcyADyRRgvbztRjiQmDnGtiVhx8pEKfXNqcOj
Q0cqXDAeJ0XP5gTicb8uzigMUFeMCK5yXrJb8hm+reBpWLE44EwkHHgTNiK6J+wq
rikgxS4j+BqlN0q+SfJWbyxVG2KaCjoTGjW5Rahl9f1gw2lLb7Lq1TkLIZ9nzPPK
+P03pGDe3d+2vOuTDHT+AYNc0o9+8ijew72siMfw4DIlsge4hS4G8fQWxuWPUqHa
DSG0HsFlG9bZ1Jajt4V2sSoLSwqpyXylB7Nr/d/fWK/5Fnbg67zP0SuMRyvFRpkS
anUvYcmPAc1Kwun2czTeX9x8n6kLaejqcqSe++2V5Z73FwMs/fQiOrTIaR/H7F5s
SRzsj6G20iPkMo12HrsEg7fOcRq9fXgzAZwVMVihSWRsJETLHouyb5Gdiz0Ks7i2
hFTx5gXxczasCOFGmj3WyrCFN7GbEU7VE0SwFR8hQmMExLvlNxNibSFSrebqAPuH
+jBGCd2KNKcxeYezqxYQfNiGtFN65XxI+JfZKdyisH0RTXDRZmKmfLsymulP4clL
zVOaElOA6DRS1BjJ+aR18aONWt8CdAT9zXNEMADbv6LaDYaWFxiosDm1H47V7fb/
6R7NTjs0vdBs9J1X6el/9yFsMnn+OqejYOJsESQB+h6ZxL/jJRtiB25gx9uS009V
ZH2QWPTA+y9qHco/sPHk2poafpOtcBo5uQ5E9hpAyLdO+XFTxmMIjWTF7Mw3EHQC
YLRJYfEfKLzKvBNNCvuO3/NJ4QuSgQqd6ZN5aDsOr86R4qLEqtnq3nqjHzf4l15b
adYQcbDwTMl4ftIwuAXusZkavkzTo3QkROropEBDCIHYZOZ6Z5QFrP74BkfLXS5i
3aCoI3tuVvVw4xmh+8H4rf6N9RYhgFMPVoIgR/5645M1Ggi8DWMxNSG3pRnNWfSV
zCjT7Magiypx7ZK5P8FS8qGEYhXBLbCEVDfraGUkwGjlRD9iixaU1NmjhP+wWMlO
V8rW6bzo2cc4EJlgjRvpYthyzD0rYdZkyDzOQ5zfmSjL1Tqy4Jg53IHlhpHMdFtn
xLcCJOMbBKLZix692eWLqNRVcMbiTQI00Prg3rtXjDbI/f5oMyiPw28W1SYl3dF5
JQpLy6Dh44QNmpbk13HpHxkWBVx5mWNUOKC+36UBqdfA9N0Fx9eV5z/yuMfUg41s
ZiWl4Xn/fj7og52gSOQLx9I5UYFgkgfNDp6n2n/U66TFGJm/ORL8O4PNkqdiGcHh
uCFt+CYU8PveHeByQPBq0PX4B9hjVPIe0btcdGe3oDEQAywvGfFi7y4CdOriffa5
en9PdZ3N4vfhD/OUTKGgnjIUlt02uxO2J7cnpqjXp0EmNCk8djFSXzftx1bI/vzz
l1uY6YucFquAyAW07lL0QwxPEGaagInSCIHQvyOLHsRe7+b0gF0ZYgCBypmHzxqc
YHZqX+Ji0/n+20C4YXLdadWpDJ+la2Jo9H7/bhv/mzzvmKzSzI8tPu2mWyRul2jm
CyasZY7j3xiq4eB7LQt1/b3wQiuAZnK1OLO3+znYX8s0mxYyTRs7SA==
-----END RSA PRIVATE KEY-----
";

                MemoryStream buf = new MemoryStream(Encoding.UTF8.GetBytes(privateKey));
                PrivateKeyFile privateKeyFile = new PrivateKeyFile(buf, ConfigurationManager.AppSettings["Passphrase"]);
                PrivateKeyConnectionInfo connectionInfo = new PrivateKeyConnectionInfo(ConfigurationManager.AppSettings["Host"], int.Parse(ConfigurationManager.AppSettings["Port"]), ConfigurationManager.AppSettings["Username"], privateKeyFile);

                using (SftpClient client = new SftpClient(connectionInfo))
                {
                    client.Connect();
                    using (FileStream targetFile = new FileStream(Path.Combine(archiveDirectory, archiveSourceFile), FileMode.Create))
                    {
                        client.ChangeDirectory(ConfigurationManager.AppSettings["SourceFileDirectory"]);
                        string sourceFileName = ConfigurationManager.AppSettings["SourceFileName"];
                        if (client.Exists(sourceFileName))
                        {
                            client.DownloadFile(sourceFileName, targetFile);
                            client.DeleteFile(sourceFileName);
                        }
                        else
                        {
                            if (File.Exists(Path.Combine(archiveDirectory, archiveSourceFile)))
                                Logger.LogError($"File '{sourceFileName}' was not found on the server.  Attempting to use local version.");
                            else
                            {
                                Logger.LogError($"File '{sourceFileName}' was not found on the server.  No local version available to use.  Process terminating.");
                                client.Disconnect();
                                return;
                            }
                        }
                    }
                    client.Disconnect();
                }

                // Generate Report
                CartStatusReport report = new CartStatusReport();
                report.QuerySKUsFromSourceFile(Path.Combine(archiveDirectory, archiveSourceFile));
                report.BuildReport();

                // Mail Report
                ReportMailer.Mail(report.OutputFileLocation);
                
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                ReportMailer.MailError(ex);
            }
        }
    }
}
