﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CartStatusReporter.Utilities
{
    public static class Logger
    {
        private const string ERROR_LOG_FILENAME = "ErrorLog.txt";
        private const string EVENT_LOG_FILENAME = "EventLog.txt";

        public static string ErrorLogFile { get => ERROR_LOG_FILENAME; }
        public static string EventLogFile { get => EVENT_LOG_FILENAME; }

        public static void LogError(string message)
        {
            string errorLogDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            Log(errorLogDirectory, ERROR_LOG_FILENAME, message);
        }

        public static void LogError(Exception ex)
        {
            LogError($"{GetErrorMessage(ex)}:  {ex.StackTrace}");
        }

        private static string GetErrorMessage(Exception ex)
        {
            StringBuilder returnMessage = new StringBuilder();
            returnMessage.AppendLine(ex.Message);
            if (ex.InnerException != null)
            {
                returnMessage.AppendLine($"Inner Exception: {GetErrorMessage(ex.InnerException)}");
            }
            return returnMessage.ToString();
        }

        public static void LogEvent(string message)
        {
            string eventLogDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            Log(eventLogDirectory, EVENT_LOG_FILENAME, message);
        }

        private static void Log(string logDirectory, string logFileName, string message)
        {
            string logFile = Path.Combine(logDirectory, logFileName);
            string logEntry = $"[{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}]:  {message}";
            if (!File.Exists(logFile))
            {
                if (!Directory.Exists(logDirectory))
                {
                    Directory.CreateDirectory(logDirectory);
                }
                using (StreamWriter sw = File.CreateText(logFile))
                {
                    sw.Write(logEntry);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(logFile))
                {
                    sw.Write($"{Environment.NewLine}{logEntry}");
                }
            }
        }
    }
}
