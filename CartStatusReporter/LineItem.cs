﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CartStatusReporter
{
    class LineItem
    {
        private string sku;
        private string description;
        private string fulfillmentSource;
        private string quantityOH;
        private string vendorCode;
        private string vendor;

        public string SKU { get => sku; set => sku = value; }
        public string FulfillmentSource { get => fulfillmentSource; set => fulfillmentSource = value; }
        public string Description { get => description; set => description = value; }        
        public string Vendor { get => vendor; set => vendor = value; }
        public string QuantityOH { get => quantityOH; set => quantityOH = value; }
        public string VendorCode { get => vendorCode; set => vendorCode = value; }

        public LineItem(string sku, string description, string fulfillmentSource, string quantityOH, string vendorCode, string vendor)
        {
            this.sku = sku;
            this.description = description;
            this.fulfillmentSource = fulfillmentSource;
            this.quantityOH = quantityOH;
            this.vendorCode = vendorCode;
            this.vendor = vendor;
        }

        public override string ToString()
        {
            return $"{sku},{fulfillmentSource}";
        }
    }
}
