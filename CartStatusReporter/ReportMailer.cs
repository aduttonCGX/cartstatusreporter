﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MimeKit;
using MailKit.Net.Smtp;
using System.IO;
using CartStatusReporter.Utilities;
using System.Configuration;

namespace CartStatusReporter
{
    public static class ReportMailer
    {
        public static void Mail(string attachmentFilePath)
        {
            try
            {
                var message = new MimeMessage();
                message.Subject = $"Cart Status Report [{DateTime.Now.ToShortDateString()}]";
                message.From.Add(new MailboxAddress("CGES Ecommerce", ConfigurationManager.AppSettings["FromEmailAddress"]));
#if DEBUG
                message.To.Add(new MailboxAddress(ConfigurationManager.AppSettings["TestToEmailAddress"]));
#else
                message.To.Add(new MailboxAddress(ConfigurationManager.AppSettings["ToEmailAddress"]));
                message.Cc.Add(new MailboxAddress(ConfigurationManager.AppSettings["TestToEmailAddress"]));
#endif
                var body = new TextPart("plain") { Text = @"This an automated message.  Please do not reply to this email." };
                var attachment = new MimePart("text", "xlsx")
                {
                    Content = new MimeContent(File.OpenRead(attachmentFilePath), ContentEncoding.Default),
                    ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
                    ContentTransferEncoding = ContentEncoding.Base64,
                    FileName = Path.GetFileName(attachmentFilePath)
                };

                var multipart = new Multipart("mixed");
                multipart.Add(body);
                multipart.Add(attachment);

                message.Body = multipart;


                using (var client = new SmtpClient())
                {
                    client.Connect(ConfigurationManager.AppSettings["SMTPServerAddress"], 25, MailKit.Security.SecureSocketOptions.None);
                    client.Send(message);
                    client.Disconnect(true);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
        }
        public static void MailError(Exception exception)
        {
            try
            {
                var message = new MimeMessage();
                message.Subject = $"Cart Status Report Failed:  {exception.Message}";
                message.From.Add(new MailboxAddress("CGES Ecommerce", ConfigurationManager.AppSettings["FromEmailAddress"]));
                message.To.Add(new MailboxAddress(ConfigurationManager.AppSettings["TestToEmailAddress"]));
                string errorMessage = $"{exception.Message}{Environment.NewLine}{Environment.NewLine}{exception.StackTrace}{Environment.NewLine}{Environment.NewLine}";
                string innerMessage = (exception.InnerException != null) ? $"{exception.InnerException.Message}{Environment.NewLine}{Environment.NewLine}{exception.InnerException.StackTrace}" : "";
                var body = new TextPart("plain") { Text = $"{errorMessage}{innerMessage}" };
                var multipart = new Multipart("mixed");
                multipart.Add(body);
                message.Body = multipart;

                using (var client = new SmtpClient())
                {
                    client.Connect(ConfigurationManager.AppSettings["SMTPServerAddress"], 25, MailKit.Security.SecureSocketOptions.None);
                    client.Send(message);
                    client.Disconnect(true);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
        }
    }
}
