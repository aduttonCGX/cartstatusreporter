﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Data.SqlClient;
using CartStatusReporter.Utilities;
using Microsoft.Office.Interop.Excel;
using System.Drawing;
using System.Diagnostics;
using System.Linq;
using System.Configuration;

namespace CartStatusReporter
{
    public class CartStatusReport
    {
        private List<LineItem> notBuiltItems;
        private List<LineItem> storeItems;        
        private List<LineItem> vendorItems;
        private List<LineItem> warehouseItems;
        private string outputFileLocation;

        public string OutputFileLocation { get => outputFileLocation; set => outputFileLocation = value; }

        public void QuerySKUsFromSourceFile(string filePath)
        {
            if (!File.Exists(filePath) || string.IsNullOrWhiteSpace(filePath))
            {
                throw new FileNotFoundException();
            }            

            notBuiltItems = new List<LineItem>();
            storeItems = new List<LineItem>();
            vendorItems = new List<LineItem>();
            warehouseItems = new List<LineItem>();

            string[] lines = File.ReadAllLines(filePath);
            List<string> formattedLines = new List<string>();
            foreach (string line in lines)
            {
                string formattedLine = line.Replace("\"", string.Empty);
                if (Int64.TryParse(formattedLine, out Int64 j))
                    formattedLines.Add(formattedLine);
            }
            string queryInClause = "IN ('" + string.Join("', '", formattedLines) + "')";

            #region Warehouse Data
            string warehouseQuery = $@"SELECT DISTINCT
	                                    p.BaseCode AS 'SKU',
	                                    p.[Description] AS 'Description',
	                                    'Warehouse' AS 'FulfillmentSource',	
	                                    SUM(CONVERT(DECIMAL(10,0), wst.Quantity)) AS 'QuantityOH',
	                                    p.VendorCode AS 'Vendor Code',
	                                    v.[Description] AS 'Vendor'
                                    FROM 
                                        [mi9_mms_cg_live].[dbo].[ar_v_Products] p
	                                    LEFT JOIN [mi9_mms_cg_live].[dbo].[ar_v_WebProducts] wp ON wp.ProductID = p.BaseID 
	                                    LEFT JOIN [mi9_mms_cg_live].[dbo].[ar_v_Vendors] v ON v.Code = p.VendorCode	
	                                    JOIN [mi9_mms_cg_live].[dbo].[productcodes] pc ON p.BaseCode = pc.variantcode
	                                    JOIN [mi9_mms_cg_live].[dbo].[supproducts] sp ON sp.varint = pc.varint
	                                    JOIN [mi9_mms_cg_live].[dbo].[ar_v_WhStocks] wst ON wst.ProductID = p.ID
                                    WHERE
                                        p.BaseCode {queryInClause}
	                                    AND wp.CorporateFulfillmentType = 2
	                                    AND wst.WarehouseCode = '992'
                                    GROUP BY
	                                    p.BaseCode,
	                                    p.[Description],
	                                    p.VendorCode,
	                                    v.[Description]
                                    ORDER BY 'Vendor Code', p.BaseCode";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CGESMERCHDB"].ConnectionString))
                {
                    conn.Open();
                    SqlCommand sqlcommand = new SqlCommand(warehouseQuery, conn);
                    SqlDataReader reader = sqlcommand.ExecuteReader();

                    while (reader.Read())
                    {
                        string sku = reader["SKU"].ToString();
                        string description = reader["Description"].ToString();
                        string fulfillmentSource = reader["FulfillmentSource"].ToString();
                        string quantityOH = reader["QuantityOH"].ToString();
                        string vendorCode = reader["Vendor Code"].ToString();
                        string vendor = reader["Vendor"].ToString();
                        warehouseItems.Add(new LineItem(sku, description, fulfillmentSource, quantityOH, vendorCode, vendor));
                    }
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            #endregion

            #region Store Data
            string storeQuery = $@"SELECT DISTINCT
	                                p.BaseCode AS 'SKU',
	                                p.[Description] AS 'Description',
	                                'Store' AS 'FulfillmentSource',	
	                                SUM(CONVERT(DECIMAL(10,0), (stk.Retail + stk.Showroom + stk.Damaged + stk.Allocated + stk.Quarantine))) AS 'QuantityOH',
	                                p.VendorCode AS 'Vendor Code',
	                                v.[Description] AS 'Vendor'
                                FROM 
                                    [mi9_mms_cg_live].[dbo].[ar_v_Products] p
	                                LEFT JOIN [mi9_mms_cg_live].[dbo].[ar_v_WebProducts] wp ON wp.ProductID = p.BaseID 
	                                LEFT JOIN [mi9_mms_cg_live].[dbo].[ar_v_Vendors] v ON v.Code = p.VendorCode	
	                                JOIN [mi9_mms_cg_live].[dbo].[productcodes] pc ON p.BaseCode = pc.variantcode
	                                JOIN [mi9_mms_cg_live].[dbo].[supproducts] sp ON sp.varint = pc.varint
	                                JOIN [mi9_mms_cg_live].[dbo].[ar_v_StoreStocks] stk ON stk.ProductCodeID = p.ID
                                WHERE
                                    p.BaseCode {queryInClause}
	                                AND wp.CorporateFulfillmentType = 3
	                                AND stk.StoreCode IN('901', '403', '459')
                                GROUP BY
	                                p.BaseCode,
	                                p.[Description],
	                                p.VendorCode,
	                                v.[Description]
                                ORDER BY 'Vendor Code', p.BaseCode";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CGESMERCHDB"].ConnectionString))
                {
                    conn.Open();
                    SqlCommand sqlcommand = new SqlCommand(storeQuery, conn);
                    SqlDataReader reader = sqlcommand.ExecuteReader();

                    while (reader.Read())
                    {
                        string sku = reader["SKU"].ToString();
                        string description = reader["Description"].ToString();
                        string fulfillmentSource = reader["FulfillmentSource"].ToString();
                        string quantityOH = reader["QuantityOH"].ToString();
                        string vendorCode = reader["Vendor Code"].ToString();
                        string vendor = reader["Vendor"].ToString();
                        storeItems.Add(new LineItem(sku, description, fulfillmentSource, quantityOH, vendorCode, vendor));
                    }
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            #endregion

            #region Vendor Data
            string vendorQuery = $@"SELECT DISTINCT
	                                    p.BaseCode AS 'SKU',
	                                    p.[Description] AS 'Description',
	                                    'Vendor' AS 'FulfillmentSource',
	                                    SUM(sp.[AvailableForWebFF]) AS 'QuantityOH',
	                                    p.VendorCode AS 'Vendor Code',
	                                    v.[Description] AS 'Vendor'
                                    FROM 
                                        [mi9_mms_cg_live].[dbo].[ar_v_Products] p
	                                    LEFT JOIN [mi9_mms_cg_live].[dbo].[ar_v_WebProducts] wp ON wp.ProductID = p.BaseID 
	                                    LEFT JOIN [mi9_mms_cg_live].[dbo].[ar_v_Vendors] v ON v.Code = p.VendorCode	
	                                    JOIN [mi9_mms_cg_live].[dbo].[productcodes] pc ON p.BaseCode = pc.variantcode
	                                    JOIN [mi9_mms_cg_live].[dbo].[supproducts] sp ON sp.varint = pc.varint
                                    WHERE
                                        p.BaseCode {queryInClause}
	                                    AND wp.CorporateFulfillmentType = 1
                                    GROUP BY
	                                    p.BaseCode,
	                                    p.[Description],
	                                    p.VendorCode,
	                                    v.[Description]
                                    ORDER BY 'Vendor Code', p.BaseCode";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CGESMERCHDB"].ConnectionString))
                {
                    conn.Open();
                    SqlCommand sqlcommand = new SqlCommand(vendorQuery, conn);
                    SqlDataReader reader = sqlcommand.ExecuteReader();

                    while (reader.Read())
                    {
                        string sku = reader["SKU"].ToString();
                        string description = reader["Description"].ToString();
                        string fulfillmentSource = reader["FulfillmentSource"].ToString();
                        string quantityOH = reader["QuantityOH"].ToString();
                        string vendorCode = reader["Vendor Code"].ToString();
                        string vendor = reader["Vendor"].ToString();
                        vendorItems.Add(new LineItem(sku, description, fulfillmentSource, quantityOH, vendorCode, vendor));
                    }
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            #endregion

            #region Not Built Data
            string notBuiltQuery = $@"SELECT DISTINCT
	                                        p.BaseCode AS 'SKU',
	                                        p.[Description] AS 'Description',
	                                        'Not built as a Web Item' AS 'FulfillmentSource',
	                                        p.VendorCode AS 'Vendor Code',
	                                        v.[Description] AS 'Vendor'
                                        FROM 
                                            [mi9_mms_cg_live].[dbo].[ar_v_Products] p
	                                        LEFT JOIN [mi9_mms_cg_live].[dbo].[ar_v_WebProducts] wp ON wp.ProductID = p.BaseID 
	                                        LEFT JOIN [mi9_mms_cg_live].[dbo].[ar_v_Vendors] v ON v.Code = p.VendorCode	
                                        WHERE
                                            p.BaseCode {queryInClause}
	                                        AND wp.CorporateFulfillmentType IS NULL
                                        GROUP BY
	                                        p.BaseCode,
	                                        p.[Description],
	                                        p.VendorCode,
	                                        v.[Description]
                                        ORDER BY 'Vendor Code', p.BaseCode";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CGESMERCHDB"].ConnectionString))
                {
                    conn.Open();
                    SqlCommand sqlcommand = new SqlCommand(notBuiltQuery, conn);
                    SqlDataReader reader = sqlcommand.ExecuteReader();

                    while (reader.Read())
                    {
                        string sku = reader["SKU"].ToString();
                        string description = reader["Description"].ToString();
                        string fulfillmentSource = reader["FulfillmentSource"].ToString();
                        string vendorCode = reader["Vendor Code"].ToString();
                        string vendor = reader["Vendor"].ToString();
                        notBuiltItems.Add(new LineItem(sku, description, fulfillmentSource, "N/A", vendorCode, vendor));
                    }
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
                #endregion
        }
        public void BuildReport()
        {
            {
                string directory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\Archive\\" + DateTime.Now.ToString(@"yyyy\\MM\\dd");
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
                string fileName = $"CartStatusReport_{DateTime.Today.ToString("yyyyMMdd")}_000.xlsx";

                // Make sure you're writing to a new file
                Directory.CreateDirectory(directory);
                int fileNumber = 0;
                while (File.Exists(Path.Combine(directory, fileName)))
                {
                    fileNumber++;
                    fileName = $"CartStatusReport_{DateTime.Today.ToString("yyyyMMdd")}_{fileNumber.ToString("D" + 3)}.xlsx";
                }

                try
                {
                    // Start Excel
                    Application excel = new Application();
                    excel.Visible = false;
                    excel.DisplayAlerts = false;
                    Workbook workbook = excel.Workbooks.Add(Type.Missing);
                    Worksheet worksheet = workbook.ActiveSheet;
                    worksheet.Name = $"CartStatusReport";
                    Range headerRange, formatRange, itemRowRange;

                    // Margins 
                    worksheet.PageSetup.Orientation = XlPageOrientation.xlPortrait;                    
                    worksheet.PageSetup.TopMargin = excel.InchesToPoints(0.75);
                    worksheet.PageSetup.BottomMargin = excel.InchesToPoints(0.75);
                    worksheet.PageSetup.LeftMargin = excel.InchesToPoints(0.25);
                    worksheet.PageSetup.RightMargin = excel.InchesToPoints(0.25);                    
                    worksheet.PageSetup.CenterHorizontally = true;

                    // Column widths
                    worksheet.Columns["A:A"].ColumnWidth = 13;
                    worksheet.Columns["B:B"].ColumnWidth = 27;
                    worksheet.Columns["C:C"].ColumnWidth = 3;
                    worksheet.Columns["D:D"].ColumnWidth = 17;
                    worksheet.Columns["E:E"].ColumnWidth = 7;
                    worksheet.Columns["F:F"].ColumnWidth = 27;
                    
                    // Report Header
                    headerRange = (Range)worksheet.Range[worksheet.Cells[1, 1], worksheet.Cells[1, 6]];
                    headerRange.Merge();
                    headerRange.HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    headerRange.VerticalAlignment = XlVAlign.xlVAlignCenter;
                    headerRange.Font.Size = 20;
                    headerRange.Font.Bold = true;
                    headerRange.Font.Color = ColorTranslator.ToOle(Color.White);
                    headerRange.Interior.Color = ColorTranslator.ToOle(Color.Gray);
                    headerRange.RowHeight = 36;
                    worksheet.Cells[1, 1] = "Cart Status Report";
                    int row = 3;

                    // Warehouse section
                    if (warehouseItems.Count > 0)
                    {
                        // Title
                        headerRange = (Range)worksheet.Range[worksheet.Cells[row, 1], worksheet.Cells[row, 6]];
                        headerRange.Merge();
                        headerRange.HorizontalAlignment = XlHAlign.xlHAlignCenter;
                        headerRange.VerticalAlignment = XlVAlign.xlVAlignCenter;
                        headerRange.Font.Size = 14;
                        headerRange.Font.Bold = true;
                        headerRange.Interior.Color = ColorTranslator.ToOle(Color.Silver);
                        headerRange.RowHeight = 18;
                        worksheet.Cells[row, 1] = $"Warehouse-Fulfilled Items ({warehouseItems.Count.ToString("N0")})";
                        row++;

                        // Column headers
                        formatRange = (Range)worksheet.Range[worksheet.Cells[row, 1], worksheet.Cells[row, 6]];
                        formatRange.HorizontalAlignment = XlHAlign.xlHAlignLeft;
                        formatRange.Font.Underline = true;
                        formatRange.Font.Bold = true;
                        worksheet.Cells[row, 1] = "SKU";
                        worksheet.Cells[row, 2] = "Description";
                        worksheet.Cells[row, 4] = "Fulfillment Source";
                        worksheet.Cells[row, 5] = "Qty OH";
                        worksheet.Cells[row, 6] = "Vendor";

                        // Data
                        for (int item = 0; item < warehouseItems.Count; item++)
                        {
                            row++;
                            itemRowRange = (Range)worksheet.Range[worksheet.Cells[row, 1], worksheet.Cells[row, 6]];
                            itemRowRange.Font.Size = 10;
                            itemRowRange.HorizontalAlignment = XlHAlign.xlHAlignLeft;
                            itemRowRange.VerticalAlignment = XlVAlign.xlVAlignTop;
                            itemRowRange.NumberFormat = "@";
                            itemRowRange.WrapText = true;
                            if (item % 2 == 1)
                            {
                                itemRowRange.Interior.Color = ColorTranslator.ToOle(Color.Gainsboro);
                            }
                            worksheet.Cells[row, 1] = warehouseItems[item].SKU;
                            worksheet.Cells[row, 2] = warehouseItems[item].Description;
                            worksheet.Cells[row, 4] = warehouseItems[item].FulfillmentSource;
                            worksheet.Cells[row, 5].NumberFormat = "0";
                            worksheet.Cells[row, 5] = warehouseItems[item].QuantityOH;
                            worksheet.Cells[row, 6] = $"{warehouseItems[item].VendorCode} - {warehouseItems[item].Vendor}";
                        }
                        row += 2;
                    }

                    // Store section
                    if (storeItems.Count > 0)
                    {
                        // Title
                        headerRange = (Range)worksheet.Range[worksheet.Cells[row, 1], worksheet.Cells[row, 6]];
                        headerRange.Merge();
                        headerRange.HorizontalAlignment = XlHAlign.xlHAlignCenter;
                        headerRange.VerticalAlignment = XlVAlign.xlVAlignCenter;
                        headerRange.Font.Size = 14;
                        headerRange.Font.Bold = true;
                        headerRange.Interior.Color = ColorTranslator.ToOle(Color.Silver);
                        headerRange.RowHeight = 18;
                        worksheet.Cells[row, 1] = $"Store-Fulfilled Items ({storeItems.Count.ToString("N0")})";
                        row++;

                        // Column headers
                        formatRange = (Range)worksheet.Range[worksheet.Cells[row, 1], worksheet.Cells[row, 6]];
                        formatRange.HorizontalAlignment = XlHAlign.xlHAlignLeft;
                        formatRange.Font.Bold = true;
                        formatRange.Font.Underline = true;
                        worksheet.Cells[row, 1] = "SKU";
                        worksheet.Cells[row, 2] = "Description";
                        worksheet.Cells[row, 4] = "Fulfillment Source";
                        worksheet.Cells[row, 5] = "Qty OH";
                        worksheet.Cells[row, 6] = "Vendor";

                        // Data
                        for (int item = 0; item < storeItems.Count; item++)
                        {
                            row++;
                            itemRowRange = (Range)worksheet.Range[worksheet.Cells[row, 1], worksheet.Cells[row, 6]];
                            itemRowRange.Font.Size = 10;
                            itemRowRange.HorizontalAlignment = XlHAlign.xlHAlignLeft;
                            itemRowRange.VerticalAlignment = XlVAlign.xlVAlignTop;
                            itemRowRange.NumberFormat = "@";
                            itemRowRange.WrapText = true;
                            if (item % 2 == 1)
                            {
                                itemRowRange.Interior.Color = ColorTranslator.ToOle(Color.Gainsboro);
                            }
                            worksheet.Cells[row, 1] = storeItems[item].SKU;
                            worksheet.Cells[row, 2] = storeItems[item].Description;
                            worksheet.Cells[row, 4] = storeItems[item].FulfillmentSource;
                            worksheet.Cells[row, 5].NumberFormat = "0";
                            worksheet.Cells[row, 5] = storeItems[item].QuantityOH;
                            worksheet.Cells[row, 6] = $"{storeItems[item].VendorCode} - {storeItems[item].Vendor}";
                        }
                        row += 2;
                    }

                    // Vendor section
                    if (vendorItems.Count > 0)
                    {
                        // Title
                        headerRange = (Range)worksheet.Range[worksheet.Cells[row, 1], worksheet.Cells[row, 6]];
                        headerRange.Merge();
                        headerRange.HorizontalAlignment = XlHAlign.xlHAlignCenter;
                        headerRange.VerticalAlignment = XlVAlign.xlVAlignCenter;
                        headerRange.Font.Size = 14;
                        headerRange.Font.Bold = true;
                        headerRange.Interior.Color = ColorTranslator.ToOle(Color.Silver);
                        headerRange.RowHeight = 18;
                        worksheet.Cells[row, 1] = $"Vendor-Fulfilled Items ({vendorItems.Count.ToString("N0")})";
                        row++;

                        // Column headers
                        formatRange = (Range)worksheet.Range[worksheet.Cells[row, 1], worksheet.Cells[row, 6]];
                        formatRange.HorizontalAlignment = XlHAlign.xlHAlignLeft;
                        formatRange.Font.Bold = true;
                        formatRange.Font.Underline = true;
                        worksheet.Cells[row, 1] = "SKU";
                        worksheet.Cells[row, 2] = "Description";
                        worksheet.Cells[row, 4] = "Fulfillment Source";
                        worksheet.Cells[row, 5] = "Qty OH";
                        worksheet.Cells[row, 6] = "Vendor";

                        // Data
                        for (int item = 0; item < vendorItems.Count; item++)
                        {
                            row++;
                            itemRowRange = (Range)worksheet.Range[worksheet.Cells[row, 1], worksheet.Cells[row, 6]];
                            itemRowRange.Font.Size = 10;
                            itemRowRange.HorizontalAlignment = XlHAlign.xlHAlignLeft;
                            itemRowRange.VerticalAlignment = XlVAlign.xlVAlignTop;
                            itemRowRange.NumberFormat = "@";
                            itemRowRange.WrapText = true;
                            if (item % 2 == 1)
                            {
                                itemRowRange.Interior.Color = ColorTranslator.ToOle(Color.Gainsboro);
                            }
                            worksheet.Cells[row, 1] = vendorItems[item].SKU;
                            worksheet.Cells[row, 2] = vendorItems[item].Description;
                            worksheet.Cells[row, 4] = vendorItems[item].FulfillmentSource;
                            worksheet.Cells[row, 5].NumberFormat = "0";
                            worksheet.Cells[row, 5] = vendorItems[item].QuantityOH;
                            worksheet.Cells[row, 6] = $"{vendorItems[item].VendorCode} - {vendorItems[item].Vendor}";
                        }
                        row += 2;
                    }

                    // Not Built section
                    if (notBuiltItems.Count > 0)
                    {
                        // Title
                        headerRange = (Range)worksheet.Range[worksheet.Cells[row, 1], worksheet.Cells[row, 6]];
                        headerRange.Merge();
                        headerRange.HorizontalAlignment = XlHAlign.xlHAlignCenter;
                        headerRange.VerticalAlignment = XlVAlign.xlVAlignCenter;
                        headerRange.Font.Size = 14;
                        headerRange.Font.Bold = true;
                        headerRange.Interior.Color = ColorTranslator.ToOle(Color.Silver);
                        headerRange.RowHeight = 18;
                        worksheet.Cells[row, 1] = $"Not Built as Web Items ({notBuiltItems.Count.ToString("N0")})";
                        row++;

                        // Column headers
                        formatRange = (Range)worksheet.Range[worksheet.Cells[row, 1], worksheet.Cells[row, 6]];
                        formatRange.HorizontalAlignment = XlHAlign.xlHAlignLeft;
                        formatRange.Font.Bold = true;
                        formatRange.Font.Underline = true;
                        worksheet.Cells[row, 1] = "SKU";
                        worksheet.Cells[row, 2] = "Description";
                        worksheet.Cells[row, 4] = "Fulfillment Source";
                        worksheet.Cells[row, 5].NumberFormat = "0";
                        worksheet.Cells[row, 5] = "Qty OH";
                        worksheet.Cells[row, 6] = "Vendor";

                        // Data
                        for (int item = 0; item < notBuiltItems.Count; item++)
                        {
                            row++;
                            itemRowRange = (Range)worksheet.Range[worksheet.Cells[row, 1], worksheet.Cells[row, 6]];
                            itemRowRange.Font.Size = 10;
                            itemRowRange.HorizontalAlignment = XlHAlign.xlHAlignLeft;
                            itemRowRange.VerticalAlignment = XlVAlign.xlVAlignTop;
                            itemRowRange.NumberFormat = "@";
                            itemRowRange.WrapText = true;
                            if (item % 2 == 1)
                            {
                                itemRowRange.Interior.Color = ColorTranslator.ToOle(Color.Gainsboro);
                            }
                            worksheet.Cells[row, 1] = notBuiltItems[item].SKU;
                            worksheet.Cells[row, 2] = notBuiltItems[item].Description;
                            worksheet.Cells[row, 4] = notBuiltItems[item].FulfillmentSource;
                            worksheet.Cells[row, 5] = notBuiltItems[item].QuantityOH;
                            worksheet.Cells[row, 6] = $"{notBuiltItems[item].VendorCode} - {notBuiltItems[item].Vendor}";
                        }
                        row += 2;
                    }

                    // Page Numbering
                    worksheet.PageSetup.LeftFooter = "Cart Status Report";
                    worksheet.PageSetup.CenterFooter = "&P/&N";
                    worksheet.PageSetup.RightFooter = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");

                    // Save and close
                    outputFileLocation = Path.Combine(directory, fileName);
                    workbook.SaveAs(outputFileLocation);
                    workbook.Close();
                    excel.Quit();
                }
                catch (Exception ex)
                {
                    throw ex;
                }               
            }
        }
    }
}
