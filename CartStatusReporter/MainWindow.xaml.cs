﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using Microsoft.Win32;
using CartStatusReporter.Utilities;
using System.IO;
using System.Diagnostics;

namespace CartStatusReporter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private bool buttonDepressed = false;
        public MainWindow()
        {
            InitializeComponent();            
        }
        private void btn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            switch (e.ChangedButton)
            {
                case MouseButton.Left:
                    Button currentButton = sender as Button;
                    currentButton.ClearValue(EffectProperty);

                    Thickness m = currentButton.Margin;
                    m.Top = currentButton.Margin.Top + (float)3;
                    currentButton.Margin = m;
                    buttonDepressed = true;
                    break;
                default:
                    break;
            }
        }
        private void btn_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (buttonDepressed)
            {
                switch (e.ChangedButton)
                {
                    case MouseButton.Left:
                        Button currentButton = sender as Button;
                        Brush brush = currentButton.Background;
                        if (brush is SolidColorBrush)
                        {
                            string colorValue = ((SolidColorBrush)brush).Color.ToString();
                        }

                        currentButton.Effect = new System.Windows.Media.Effects.DropShadowEffect
                        {
                            BlurRadius = 5,
                            Direction = 315,
                            Opacity = 1,
                            ShadowDepth = 3
                        };

                        Thickness m = currentButton.Margin;
                        m.Top = currentButton.Margin.Top - (float)3;
                        currentButton.Margin = m;
                        buttonDepressed = false;
                        break;
                    default:
                        break;
                }
            }
        }
        private void BtnOpenFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = @"Downloads";
            if (openFileDialog.ShowDialog() == true)
            {
                txtSourceFileLocation.Text = openFileDialog.FileName;
            }
        }
        private void BtnRunReport_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtSourceFileLocation.Text))
            {
                try
                {
                    CartStatusReport report = new CartStatusReport();
                    report.QuerySKUsFromSourceFile(txtSourceFileLocation.Text);
                    report.BuildReport();

                    // Open the folder
                    Process.Start("explorer.exe", System.IO.Path.GetDirectoryName(report.OutputFileLocation));

                }                
                catch (Exception ex)
                {
                    Logger.LogError(ex);
                    MessageBox.Show($"Report generation failed: {ex.Message}", "Error", System.Windows.MessageBoxButton.OK);
                }
            }
        }        
    }
}
